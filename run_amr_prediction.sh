#!/bin/bash
#$ -cwd
#$ -o output.log
#$ -e error.log
#$ -P aanensen.prjc
#$ -N assembly
#$ -q long.qc
#$ -pe shmem 2
#$ -l h_vmem=8G

usage() {
  cat <<EOF
Usage: run_amr_prediction.sh -i <FASTQ_DIR> -s <SPECIES> -o <OUTPUT_DIR>.
Specifying a species with -s will search for point mutations. To show available species use 'run_amr_prediction.sh -a'
EOF
1>&2
  exit 1
}

available_species() {
  cat <<EOF
Available species:
    campylobacter
    enterococcus_faecalis
    enterococcus_faecium
    escherichia_coli
    helicobacter_pylori
    klebsiella
    mycobacterium_tuberculosis
    neisseria_gonorrhoeae
    salmonella
    staphylococcus_aureus
EOF
}

while getopts i:s:o:ah ARGS
do
    case "${ARGS}" in
        i) INPUT_DIR=${OPTARG};;
        s) SPECIES=${OPTARG};;
        o) OUTPUT_DIR=${OPTARG};;
        a) available_species
           exit 0;;
        h) usage
           exit 0;;
        \? ) usage
        exit 1;;
    esac
done

if (( $OPTIND == 1 ))
then
   usage
fi

if [ -z $INPUT_DIR ]
then
    echo "No fastq dir specified using -i"
    exit 1
fi

if [ -z $OUTPUT_DIR ]
then
    echo "No output dir specified using -o"
    exit 1
fi


export NXF_ANSI_LOG=false
export NXF_OPTS="-Xms8G -Xmx8G -Dnxf.pool.maxThreads=2000"

NEXTFLOW_WORKFLOWS_DIR='/well/aanensen/shared/nextflow_workflows'

if [ -z $SPECIES ]
then
  /well/aanensen/shared/software/bin/nextflow run \
  ${NEXTFLOW_WORKFLOWS_DIR}/amr_prediction-1.2/main.nf \
  --input_dir ${INPUT_DIR} \
  --fastq_pattern '*{R,_}{1,2}.f*q.gz' \
  --output_dir ${OUTPUT_DIR} \
  -w ${INPUT_DIR}/work \
  -profile bmrc -qs 1000 -resume
else
  /well/aanensen/shared/software/bin/nextflow run \
  ${NEXTFLOW_WORKFLOWS_DIR}/amr_prediction-1.2/main.nf \
  --input_dir ${INPUT_DIR} \
  --fastq_pattern '*{R,_}{1,2}.f*q.gz' \
  --species ${SPECIES} \
  --output_dir ${OUTPUT_DIR} \
  -w ${OUTPUT_DIR}/work \
  -profile bmrc -qs 1000 -resume
fi

# clean up on exit 0
status=$?
## take some decision ##
if [[ $status -eq 0 ]]; then
  rm -r ${INPUT_DIR}/work
fi