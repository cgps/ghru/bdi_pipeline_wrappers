#!/bin/bash
#$ -cwd
#$ -o output.log
#$ -e error.log
#$ -P aanensen.prjc
#$ -N assembly
#$ -q long.qc
#$ -pe shmem 2
#$ -l h_vmem=8G

usage() {
  cat <<EOF
Usage: run_mlst.sh -i <FASTQ_DIR> -s <SPECIES> -o <OUTPUT_DIR>.
To show available species use 'run_mlst.sh -a'
EOF
1>&2
  exit 1
}

available_species() {
  cat <<EOF
Available species:
    Achromobacter spp.
    Acinetobacter baumannii
    Aeromonas spp.
    Anaplasma phagocytophilum
    Arcobacter spp.
    Aspergillus fumigatus
    Bacillus cereus
    Bacillus licheniformis
    Bacillus subtilis
    Bartonella bacilliformis
    Bartonella henselae
    Bartonella washoensis
    Bordetella spp.
    Borrelia spp.
    Brachyspira hampsonii
    Brachyspira hyodysenteriae
    Brachyspira intermedia
    Brachyspira pilosicoli
    Brachyspira spp.
    Brucella spp.
    Burkholderia cepacia complex
    Burkholderia pseudomallei
    Campylobacter concisus curvus
    Campylobacter fetus
    Campylobacter helveticus
    Campylobacter hyointestinalis
    Campylobacter insulaenigrae
    Campylobacter jejuni
    Campylobacter lanienae
    Campylobacter lari
    Campylobacter sputorum
    Campylobacter upsaliensis
    Candida albicans
    Candida glabrata
    Candida krusei
    Candida tropicalis
    Candidatus Liberibacter solanacearum
    Carnobacterium maltaromaticum
    Chlamydiales spp.
    Citrobacter freundii
    Clonorchis sinensis
    Clostridioides difficile
    Clostridium botulinum
    Clostridium septicum
    Corynebacterium diphtheriae
    Cronobacter spp.
    Dichelobacter nodosus
    Edwardsiella spp.
    Enterobacter cloacae
    Enterococcus faecalis
    Enterococcus faecium
    Escherichia coli
    Flavobacterium psychrophilum
    Gallibacterium anatis
    Haemophilus influenzae
    Haemophilus parasuis
    Helicobacter cinaedi
    Helicobacter pylori
    Helicobacter suis
    Kingella kingae
    Klebsiella aerogenes
    Klebsiella oxytoca
    Klebsiella pneumoniae
    Kudoa septempunctata
    Lactobacillus salivarius
    Leptospira spp.
    Listeria monocytogenes
    Macrococcus canis
    Macrococcus caseolyticus
    Mannheimia haemolytica
    Melissococcus plutonius
    Moraxella catarrhalis
    Mycobacteria spp.
    Mycobacterium abscessus
    Mycobacterium massiliense
    Mycoplasma agalactiae
    Mycoplasma bovis
    Mycoplasma flocculare
    Mycoplasma hominis
    Mycoplasma hyopneumoniae
    Mycoplasma hyorhinis
    Mycoplasma iowae
    Mycoplasma pneumoniae
    Mycoplasma synoviae
    Neisseria spp.
    Orientia tsutsugamushi
    Ornithobacterium rhinotracheale
    Paenibacillus larvae
    Pasteurella multocida
    Pediococcus pentosaceus
    Photobacterium damselae
    Piscirickettsia salmonis
    Porphyromonas gingivalis
    Propionibacterium acnes
    Pseudomonas aeruginosa
    Pseudomonas fluorescens
    Pseudomonas putida
    Rhodococcus spp.
    Riemerella anatipestifer
    Salmonella enterica
    Saprolegnia parasitica
    Sinorhizobium spp.
    Staphylococcus aureus
    Staphylococcus epidermidis
    Staphylococcus haemolyticus
    Staphylococcus hominis
    Staphylococcus lugdunensis
    Staphylococcus pseudintermedius
    Stenotrophomonas maltophilia
    Streptococcus agalactiae
    Streptococcus bovis equinus complex
    Streptococcus canis
    Streptococcus dysgalactiae equisimilis
    Streptococcus gallolyticus
    Streptococcus oralis
    Streptococcus pneumoniae
    Streptococcus pyogenes
    Streptococcus suis
    Streptococcus thermophilus
    Streptococcus uberis
    Streptococcus zooepidemicus
    Streptomyces spp
    Taylorella spp.
    Tenacibaculum spp.
    Treponema pallidum
    Trichomonas vaginalis
    Ureaplasma spp.
    Vibrio cholerae
    Vibrio parahaemolyticus
    Vibrio spp.
    Vibrio tapetis
    Vibrio vulnificus
    Wolbachia
    Xylella fastidiosa
    Yersinia pseudotuberculosis
    Yersinia ruckeri
EOF
}

while getopts i:s:o:ah ARGS
do
    case "${ARGS}" in
        i) INPUT_DIR=${OPTARG};;
        s) SPECIES=${OPTARG};;
        o) OUTPUT_DIR=${OPTARG};;
        a) available_species
           exit 0;;
        h) usage
           exit 0;;
        \? ) usage
        exit 1;;
    esac
done

if (( $OPTIND == 1 ))
then
   usage
fi

if [ -z $INPUT_DIR ]
then
    echo "No fastq dir specified using -i"
    exit 1
fi

if [ -z "$SPECIES" ]
then
    echo "No species specified using -s"
    exit 1
fi

if [ -z $OUTPUT_DIR ]
then
    echo "No output dir specified using -o"
    exit 1
fi


export NXF_ANSI_LOG=false
export NXF_OPTS="-Xms8G -Xmx8G -Dnxf.pool.maxThreads=2000"

NEXTFLOW_WORKFLOWS_DIR='/well/aanensen/shared/nextflow_workflows'


/well/aanensen/shared/software/bin/nextflow run \
${NEXTFLOW_WORKFLOWS_DIR}/mlst-1.2/main.nf \
--input_dir ${INPUT_DIR} \
--fastq_pattern '*{R,_}{1,2}.f*q.gz' \
--mlst_species "${SPECIES}" \
--output_dir ${OUTPUT_DIR} \
-w ${OUTPUT_DIR}/work \
-profile bmrc -qs 1000 -resume

# clean up on exit 0
status=$?
## take some decision ##
if [[ $status -eq 0 ]]; then
  rm -r ${INPUT_DIR}/work
fi