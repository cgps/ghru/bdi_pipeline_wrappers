#!/bin/bash
#$ -cwd
#$ -o output.log
#$ -e error.log
#$ -P aanensen.prjc
#$ -N assembly
#$ -q long.qc
#$ -pe shmem 2
#$ -l h_vmem=8G

usage() {
  echo "Usage: run_snp_phylogeny.sh -i <FASTQ_DIR> -r <PATH TO REF SEQUENCE> -o <OUTPUT_DIR>" 1>&2
  exit 1
}

while getopts i:r:o:h ARGS
do
    case "${ARGS}" in
        i) INPUT_DIR=${OPTARG};;
        r) REFERENCE=${OPTARG};;
        o) OUTPUT_DIR=${OPTARG};;
        h ) usage
        exit 0;;
        \? ) usage
        exit 1;;
    esac
done

if (( $OPTIND == 1 ))
then
   usage
fi

if [ -z $INPUT_DIR ]
then
    echo "No fastq dir specified using -i"
    exit 1
fi

if [ -z $REFERENCE ]
then
    echo "No reference sequence specified using -r"
    exit 1
fi

if [ -z $OUTPUT_DIR ]
then
    echo "No output dir specified using -o"
    exit 1
fi


export NXF_ANSI_LOG=false
export NXF_OPTS="-Xms8G -Xmx8G -Dnxf.pool.maxThreads=2000"

NEXTFLOW_WORKFLOWS_DIR='/well/aanensen/shared/nextflow_workflows'

/well/aanensen/shared/software/bin/nextflow run \
${NEXTFLOW_WORKFLOWS_DIR}/snp_phylogeny-1.2.2/snp_phylogeny.nf \
--input_dir ${INPUT_DIR} \
--fastq_pattern '*{R,_}{1,2}.f*q.gz' \
--reference ${REFERENCE} \
--output_dir ${OUTPUT_DIR} \
--tree \
--simple_output \
-w ${OUTPUT_DIR}/work \
-profile bmrc -qs 1000 -resume

# clean up on exit 0
status=$?
## take some decision ##
if [[ $status -eq 0 ]]; then
  rm -r ${INPUT_DIR}/work
fi